data "aws_region" "current" {}

locals {
  app_name = "${var.app_name != "" ? var.app_name : "${var.project}-${var.environment}"}"

  tags = "${
              merge(
                var.tags,
                map("project", var.project),
                map("environment", var.environment),
                map("terraform", "true"),
                map("terraform.module", "osdx/ecs-fargate")
              )
          }"

  cloudwatch_logs_group_prefix = "/ecs/fargate/${var.environment}"

  cloudwatch_logs_group = "${var.cloudwatch_log_group}"
}

## ECS Service
resource "aws_ecs_service" "default" {
  name            = "${local.app_name}"
  cluster         = "${var.ecs_cluster}"
  task_definition = "${aws_ecs_task_definition.default.arn}"
  desired_count   = 1

  launch_type = "FARGATE"

  network_configuration {
    subnets          = ["${var.vpc_subnets}"]
    assign_public_ip = true
    security_groups  = ["${aws_security_group.default.id}"]
  }
}

## ECS Task
resource "aws_ecs_task_definition" "default" {
  family = "${local.app_name}"

  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]

  cpu    = "${var.ecs_task_cpu}"
  memory = "${var.ecs_task_memory}"

  execution_role_arn = "${var.iam_role_arn_ecs_task}"

  container_definitions = "${var.ecs_task_container_definitions}"
}

## Security group
resource "aws_security_group" "default" {
  name        = "${local.app_name}"
  description = "Managed by osdex/ecs-fargate terraform module"
  vpc_id      = "${var.vpc_id}"

  tags = "${merge(local.tags, map("Name", "${local.app_name}"))}"
}

resource "aws_security_group_rule" "default_in" {
  type        = "ingress"
  protocol    = "tcp"
  from_port   = "${var.app_port}"
  to_port     = "${var.app_port}"
  cidr_blocks = ["${var.all_cidr}"]

  security_group_id = "${aws_security_group.default.id}"
}

# task needs to (at least) pull container from docker hub
resource "aws_security_group_rule" "default_out" {
  type        = "egress"
  protocol    = "tcp"
  from_port   = 443
  to_port     = 443
  cidr_blocks = ["${var.all_cidr}"]

  security_group_id = "${aws_security_group.default.id}"
}

resource "aws_security_group_rule" "configured_out" {
  count = "${length(var.network_out_cidrs) > 0 ? 1 : 0}"

  type        = "egress"
  protocol    = "all"
  from_port   = 0
  to_port     = 0
  cidr_blocks = ["${var.network_out_cidrs}"]

  security_group_id = "${aws_security_group.default.id}"
}

## CloudWatch logs
resource "aws_cloudwatch_log_group" "default" {
  name = "${local.cloudwatch_logs_group}"

  tags = "${merge(local.tags, map("Name", "${local.app_name}"))}"
}
