#!/usr/bin/env sh

set -e
set -v

CI_VARIABLES=ci-variables.tfvars
OVERRIDES_FILE=ci-overrides.tf

echo '
aws_access_key_id = "string"
aws_secret_access_key = "string"
aws_region = "string"
cloudwatch_log_group = "string"
project = "string"
app_name = "string"
ecs_cluster = "string"
ecs_task_container_definitions = "[]"
vpc_id = "string"
vpc_subnets = ["list"]
iam_role_arn_ecs_task = "string"
environment = "string"
' > ${CI_VARIABLES}

echo 'provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}
' >  ${OVERRIDES_FILE}

terraform init
terraform validate -var-file ${CI_VARIABLES} .

rm -f ci-*.tf*
