variable "project" {}
variable "environment" {}

variable "app_name" {}

variable "app_port" {
  default = "8080"
}

variable "network_out_cidrs" {
  type    = "list"
  default = []
}

variable "all_cidr" {
  type    = "list"
  default = ["0.0.0.0/0"]
  description = "If this is changed Fargate might not be able to pull docker images from 3rd party repos"
}

variable "tags" {
  type    = "map"
  default = {}
}

variable "ecs_cluster" {}

variable ecs_task_cpu {
  type    = "string"
  default = "256"
}

variable ecs_task_memory {
  type    = "string"
  default = "512"
}

variable "ecs_task_container_definitions" {}

variable "vpc_id" {}

variable "vpc_subnets" {
  type = "list"
}

variable "iam_role_arn_ecs_task" {}

variable "cloudwatch_log_group" {}
